import React, { useState, useEffect } from 'react';
import { Map, TileLayer } from 'react-leaflet';

import GeojsonLayer from './GeojsonLayer';

import './Map.css';

const MapComponent = () => {
    const [lat, setLat] = useState(40);
    const [lng, setLng] = useState(-100);
    const [position, setPosition] = useState([lat, lng]);

    const [geojsonvisible, setGeojsonvisible] = useState(false);

    useEffect(() => {
        setPosition([lat, lng])
    }, [lat, lng])

    const changeLatHandle = (e) => {
        setLat(e.target.value)
    }

    const changeLngHandle = (e) => {
        setLng(e.target.value)
    }

    const onGeojsonToggle = (e) => {
        setGeojsonvisible(!geojsonvisible)
      }

    return (
        <>
            <input onChange={changeLatHandle} value={lat} />
            <input onChange={changeLngHandle} value={lng}/>
            <Map center={position} zoom={4}>
                <TileLayer
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                    attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
                />
                <div className="geojson-toggle">
                    <label htmlFor="layertoggle">Toggle Geojson </label>
                    <input type="checkbox"
                        name="layertoggle" id="layertoggle"
                        value={geojsonvisible} onChange={onGeojsonToggle} />
                </div>

                {geojsonvisible && <GeojsonLayer url="geojson.json" />}
            </Map>
        </>
    )
};

export { MapComponent };