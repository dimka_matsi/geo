import React from 'react';
import { GeoJSON, FeatureGroup, Popup } from 'react-leaflet';

export default class GeojsonLayer extends React.Component {
 
  state = {
    data: []
  };

  myStyle = () => {
    return {
      color: "green",
      weight: 3,
      opacity: 1,
      fillColor: "red",
      dashArray: '8 5'
    }
  }

  render() {
    return (
      <FeatureGroup>
        {this.state.data.map((f, i) => {
          return <GeoJSON key={i} data={f} style={this.myStyle}>
             <Popup>{f.properties.name}</Popup>
          </GeoJSON>
        })}
      </FeatureGroup>
    );
  }

  componentDidMount() {
    if (this.props.url) {
      this.fetchData(this.props.url);
    }
  }

  fetchData(url) {
    let request = fetch(url);

    request
      .then(r => r.json())
      .then(data => {
        this.setState({
          data: data.features
        });
      }, (error) => {
        console.error(error);
      });
  }
}